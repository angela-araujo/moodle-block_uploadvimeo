# moodle-block_uploadvimeo
Block to upload videos to vimeo

This block was created to make it easy to upload videos to an institutional Vimeo account.

It works for roles with editing permissions in the course (such as teachers, administrators, etc).
Videos are organized by folders on Vimeo for each user.

It is necessary to create an app in the Vimeo institutional account.
